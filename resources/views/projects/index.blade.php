@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="/projects" method="GET" role="search">
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search"
                                        placeholder="Search Projects"> <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <ul class="nav navbar-right panel_toolbox">
                                </li>
                                @can('Create projects')
                                    <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                                    class="fa fa-plus"></i></a>
                                    </li>
                                @endcan
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Team Members</th>
                            <th>Project progress</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th>Tribe</th>
                            <th>Squad</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{!! $project->id !!}</td>
                                <td>{!! $project->link !!}</td>
                                <td>{!! $project->members->count() !!}</td>
                                <td>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar"
                                             data-transitiongoal="{!! $project->completion_level !!}"
                                             aria-valuenow="49"
                                             style="width: {!! $project->completion_level !!}%;"></div>
                                    </div>
                                </td>
                                <td>{!! $project->status_badge !!}</td>
                                <td>{!! $project->description !!}</td>
                                <td>{!! $project->tribe_id !!}</td>
                                <td>{!! $project->squad_id !!}</td>
                                <td>
                                    @can('Delete projects')
                                        <button id="{!! $project->id !!}"
                                                class="btn btn-danger btn-xs delete-client"><i
                                                    class="fa  {!! $project->trashed()? 'fa-recycle':'fa-trash' !!}"></i>
                                        </button>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $projects->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add a project</h4>
                </div>
                {{ Form::open(['route' => 'projects.store']) }}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Project name<span class="required">*</span></label>
                        {!!  Form::text('name', $value = null, $attributes =[
                        'class' => 'form-control','placeholder' => 'Project name'
                        ]); !!}
                    </div>
                    <div class="form-group">
                        <label>Project description<span class="required">*</span></label>
                        <textarea class="form-control" name="description" placeholder="Project description"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Project</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{!! asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <script>
        $(document).ready(function () {
            $(".delete-client").on('click', function () {
                var r = confirm("Are you sure you want to delete this project?");
                if (r) {
                    $.ajax({
                        url: '{!! url('projects') !!}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting project");
                        }
                    });
                }
            });

        });
    </script>
@endsection

