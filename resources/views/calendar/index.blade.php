@extends('layouts.app')

@section('styles')
<link href="{!! asset('css/fullcalendar.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                        
                        @can('Create projects')
                            <li><a data-toggle="modal" data-target=".bs-example-modal-lg">New <i
                                            class="fa fa-plus"></i></a>
                            </li>
                        @endcan
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="table-responsive">
                    {!! $calendar->calendar() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create projects')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Create a Deployment</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('route' => 'calendar.index','method'=>'POST','files'=>'true')) !!}
        
                                <div class="form-group">
                                    {!! Form::label('title','Deployment Title:') !!}
                                    <div class="">
                                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('title', '<p class="alert alert-danger">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('title','Deployment Project:') !!}
                                    <div class="">
                                    {!! Form::select('project', $projects, null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('projects', '<p class="alert alert-danger">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('comment','Deployment Comment:') !!}
                                    <div class="">
                                    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('comment', '<p class="alert alert-danger">:message</p>') !!}
                                    </div>
                                </div>
        
                                <div class="form-group">
                                {!! Form::label('start_date','Start Date:') !!}
                                <div class="">
                                {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('start_date', '<p class="alert alert-danger">:message</p>') !!}
                                </div>
                                </div>
        
                                <div class="form-group">
                                {!! Form::label('end_date','End Date:') !!}
                                <div class="">
                                {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('end_date', '<p class="alert alert-danger">:message</p>') !!}
                                </div>
                                </div>
        
                            <div class="col-xs-1 col-sm-1 col-md-1 text-center"> &nbsp;<br/>
                            {!! Form::submit('Add Deployment',['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    @endcan
@stop

@section('scripts')
<script src="{!! asset('js/moment.min.js') !!}"></script>
<script src="{!! asset('js/fullcalendar.min.js') !!}"></script>
{!! $calendar->script() !!}
    
@endsection