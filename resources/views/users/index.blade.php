@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Portal users</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        @can('Create users')
                            <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                            class="fa fa-user-plus"></i></a>
                            </li>
                        @endcan
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>User Id</th>
                                <th>
                                    Name
                                </th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Projects</th>
                                <th>Last connected</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{!! $user->id !!}</td>
                                    <td>{!! $user->link !!}</td>
                                    <td>{!! $user->email !!}</td>
                                    <td>{!! $user->role !!}</td>
                                    <td>0</td>
                                    <td>{!! $user->updated_at->diffForHumans() !!}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create users')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Create user</h4>
                    </div>
                    {{ Form::open(['route' => 'users.store']) }}
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>First Name<span class="required">*</span></label>
                                {!!  Form::text('first_name', $value = null, $attributes =[
                                'class' => 'form-control'
                                ]); !!}
                            </div>
                            <div class="col-sm-6">
                                <label>Last Name<span class="required">*</span></label>
                                {!!  Form::text('last_name', $value = null, $attributes =[
                                'class' => 'form-control'
                                ]); !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Role<span class="required">*</span></label>
                            <select class="form-control" name="role">
                                @foreach($roles as $role)
                                    <option value="{!! $role->id !!}">{!! $role->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Email address<span class="required">*</span></label>
                            {!!  Form::email('email', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]); !!}
                        </div>
                        <div class="form-group">
                            <label>Set Password<span class="required">*</span></label>
                            {!!  Form::password( $value = null, $attributes =['name'=>'password',
                            'class' => 'form-control'
                            ]); !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create User</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
