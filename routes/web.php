<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::post('/', 'RoleController@permissionGrant')->name('permissions.grant');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/board', 'TeamBoardController');
    Route::resource('/tribes', 'TribeController');
    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/systems', 'SystemController');
    Route::get('/my_projects', 'ProjectController@myProjects')->name('projects.mine');
    Route::get('/projects/join/{user_id}/{project_id}', 'ProjectController@joinProject')->name('projects.join');
    Route::get('/projects/leave/{user_id}/{project_id}', 'ProjectController@leaveProject')->name('projects.leave');
    Route::post('/projects/update/save/{project_id}', 'ProjectController@saveUpdate')->name('projects.update.save');
    Route::resource('/projects', 'ProjectController');
    Route::resource('/calendar', 'EventController');
    Route::get('/reports/updates', 'ReportController@updatesReport')->name('reports.updates');
});
