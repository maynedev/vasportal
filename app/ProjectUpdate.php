<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectUpdate extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'project_id', 'id', 'user_id', 'description', 'title',
        'update_subject', 'completion_level', 'project_user_id', 'start_date', 'end_date'
    ];

    protected $dates = ['created_at', 'updated_at', 'start_date', 'end_date'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getLinkAttribute()
    {
        return link_to(route('projects.show', [$this->id]), $this->name);
    }
}
