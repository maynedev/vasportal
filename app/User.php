<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRoleAttribute()
    {
        $roles = $this->getRoleNames();
        return count($roles) > 0 ? $roles[0] : 'None';
    }

    public function getLinkAttribute()
    {
        return link_to(route('users.show', [$this->id]), $this->name);
    }

    public function getFirstNameAttribute()
    {
        return explode(" ", $this->name)[0];
    }

    public function getIsMeAttribute()
    {
        return Auth::user()->id == $this->id;
    }

    public function getLastNameAttribute()
    {
        $nameArray = explode(" ", $this->name);
        return count($nameArray) > 1 ? $nameArray[1] : "";
    }


    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_users');
    }


    public function updates()
    {
        return $this->hasMany(ProjectUpdate::class, 'user_id');
    }

    public function tribes()
    {
        return $this->belongsTo(Tribe::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
