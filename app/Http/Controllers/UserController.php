<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = 'Users';
        $users = User::paginate(10);
        $roles = Role::all();
        return view('users.index', compact('users', 'page_name', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'role' => 'required|exists:roles,id',
            'password' => 'required',
        ]);
        $user = User::updateOrCreate(
            [
                'email' => $request->email
            ],
            [
                'role_id' => 1,
                'name' => $request->first_name . " " . $request->last_name,
                'password' => Hash::make($request->password)
            ]
        );

        $role = Role::find($request->role);
        $user->assignRole($role);
        $request->session()->flash('success', 'User account created');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $page_name = $user->name . ' Profile';
        $roles = Role::all();
        return view('users.show', compact('user', 'page_name', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $system
     * @return \Illuminate\Http\Response
     */
    public function edit(User $system)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'role' => 'required|exists:roles,id',
            'password' => 'required',
        ]);
        $user = User::updateOrCreate(
            [
                'id' => $user->id
            ],
            [
                'role_id' => 1,
                'email' => $request->email,
                'name' => $request->first_name . " " . $request->last_name,
                'password' => Hash::make($request->password)
            ]
        );

        if ($user->role != 'None')
            $user->removeRole($user->role);

        $role = Role::find($request->role);
        $user->assignRole($role);
        $request->session()->flash('success', 'User account updated');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @return void
     * @throws \Exception
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();
        $request->session()->flash('success', 'User account deleted');
        return back();
    }
}
