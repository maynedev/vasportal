<?php

namespace App\Http\Controllers;

use Calendar;
use App\Event;
use App\Project;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = "Deployments";

        $projects = auth()->user()->projects()
            ->orderBy('name')
            ->pluck('projects.name', 'projects.id');

        $events = [];
        $data = Event::all();
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = \Calendar::event(
                    $value->title . " - " . $value->user->name,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date . ' +1 day'),
                    null,
                    // Add color and link on event
                    [
                        'color' => '#f05050',
                        'url' => url("projects/{$value->project_id}"),
                    ]
                );
            }
        }
        $calendar = \Calendar::addEvents($events);

        return view('calendar.index', compact('page_name', 'calendar', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'project' => 'required',
            'comment' => '',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $event = new Event;
        $event->title = $request['title'];
        $event->user_id = auth()->id();
        $event->project_id = $request['project'];
        $event->comment = $request['comment'];
        $event->start_date = $request['start_date'];
        $event->end_date = $request['end_date'];
        $event->save();

        $request->session()->flash('success', 'Deployment created successfully');
        return back();

        // \Session::flash('success', 'Event added successfully.');
        // return Redirect::to('/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
