<?php

namespace App\Http\Controllers;

use App\System;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $systems = System::paginate(10);
        $page_name = 'Core systems';
        $users = User::paginate(10);
        return view('systems.index', compact('systems', 'page_name', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:systems,name',
            'description' => 'sometimes',
        ]);

        System::updateOrCreate([
            'name' => $request->name,
            'description' => $request->description,
            'vendor_id' => 1
        ]);
        $request->session()->flash('success', 'System added successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\System $system
     * @return \Illuminate\Http\Response
     */
    public function show(System $system)
    {
        $page_name = $system->name . ' Profile';
        return view('systems.show', compact('system', 'page_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\System $system
     * @return \Illuminate\Http\Response
     */
    public function edit(System $system)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\System $system
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, System $system)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('systems')->ignore($system)
            ],
            'description' => 'sometimes',
        ]);

        System::updateOrCreate(
            [
                'id' => $system->id
            ],
            [
                'name' => $request->name,
                'description' => $request->description
            ]
        );
        $request->session()->flash('success', 'System added successfully');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\System $system
     * @return \Illuminate\Http\Response
     */
    public function destroy(System $system)
    {
        //
    }
}
