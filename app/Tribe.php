<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tribe extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function squads()
    {
        return $this->hasMany(Squad::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getLinkAttribute()
    {
        return link_to(route('tribes.show', [$this->id]), $this->name);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
