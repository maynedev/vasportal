<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamBoard extends Model
{
    protected $fillable = ['post', 'user_id', 'id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
